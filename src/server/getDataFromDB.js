const mysql = require('mysql');
const {con } =require('./db_conn.js');

 function getMatchPerYearData(){
        return new Promise((resolve,reject)=>{
                let sqlQuery=`SELECT season,COUNT(*) AS total_matches 
                        FROM matches 
                        GROUP BY season 
                        ORDER BY season`;
                con.query(sqlQuery, function(err,result){
                        if (err){
                                reject(err);
                        }else{
                                resolve(result);
                        }          
                });
        });
}

function getWinTeamPerYear(){
        return new Promise((resolve,reject) => {
                let sql = `SELECT  season, count(id) as matches,winner from matches group by season,winner;`;
                con.query(sql, function(err,result){
                        if (err){
                                reject(err);
                        }else{      
                                result =JSON.parse(JSON.stringify(result))
                                let winTeamPerYear = {};
                                result.forEach(function(row){
                                        if(row['season'] in winTeamPerYear){
                                               winTeamPerYear[row['season']][row['winner']] = row['matches'];
                                        }
                                        else{
                                                winTeamPerYear[row['season']] = {};
                                                winTeamPerYear[row['season']][row['winner']] = row['matches'];                                     
                                       }
                                }); 
                                resolve(winTeamPerYear);
                        }          
                });

        });
}

function getTeamExtraRun(){
        return new Promise((resolve,reject)=>{
                let sqlQuery=`SELECT bowling_team , sum(extra_runs) as extra_runs from
                        deliveries INNER JOIN matches
                        ON matches.id = deliveries.match_id
                        WHERE matches.season = 2016
                        GROUP BY deliveries.bowling_team;`;
                con.query(sqlQuery, function(err,result){
                        if (err){
                                reject(err);
                        }else{
                                result =JSON.parse(JSON.stringify(result));
                                runs_object = {};
                                result.forEach(row =>{
                                        runs_object[row['bowling_team']] = row['extra_runs'];
                                })
                                resolve(runs_object);
                        }          
                });
        });
}

function getBowlerEconomy(){
        return new Promise((resolve,reject)=>{
                let sqlQuery=`SELECT deliveries.bowler, SUM(deliveries.total_runs)/(count(*)/6) as economy 
                        from deliveries INNER JOIN matches 
                        on deliveries.match_id = matches.id 
                        WHERE matches.season = 2015
                        GROUP BY deliveries.bowler
                        ORDER BY economy
                        LIMIT 10`;
                con.query(sqlQuery, function(err,result){
                        if (err){
                                reject(err);
                        }else{
                                result =JSON.parse(JSON.stringify(result)); 
                                economy_object = {};
                                result.forEach(row =>{
                                        economy_object[row['bowler']] = row['economy'];
                                })                             
                                resolve(economy_object);
                        }          
                });
        });
}
module.exports ={
        getMatchPerYearData,
        getWinTeamPerYear,
        getTeamExtraRun,
        getBowlerEconomy
}