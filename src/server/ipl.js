const fs = require('fs'); 

/**
 * This function accepts the data as array of object from matches.csv and generates json 
 * file of number of matches played per year for all the years in IPL.
 * It also return json string of that result.
 * 
 * @param {Array.<Object>} data: csv file as array of object
 * @return {string}- result as json string
 */
let getMatchPerYear = function(data){
    if(data == undefined){
        return null;
    }
    
    let matchPerYear = {};
    data.forEach(function(row){
        if(row['season'] in matchPerYear){
            matchPerYear[row['season']] += 1;
        }
        else{
            matchPerYear[row['season']] = 1;
        }
    });

    matchPerYear = JSON.stringify(matchPerYear);
    fs.writeFile('./src/public/output/matchPerYear.json', matchPerYear, (error) => {
        if (error){
            console.log(error);
        }
    });

    return matchPerYear;
};

/**
 * This function accepts the data as array of object from matches.csv and generates 
 * json file of number of matches won per team per year in IPL.
 * It also return json string of the result.
 * 
 * @param {Array.<Object>} data: csv file as array of object
 * @return {string}- result as json string
 */
let getWinTeamPerYear = function(data){
    let winTeamPerYear = {};
    if(data == undefined){
        return null;
    }

    data.forEach(function(row){
        if(row['season'] in winTeamPerYear){
            if (row['winner'] in winTeamPerYear[row['season']] ){
                winTeamPerYear[row['season']][row['winner']] += 1;
            }
            else{
                winTeamPerYear[row['season']][row['winner']] = 1;
            }    
        }
        else{
            winTeamPerYear[row['season']] = {};
            winTeamPerYear[row['season']][row['winner']] = 1;
          
        }
    });

    winTeamPerYear = JSON.stringify(winTeamPerYear);
    fs.writeFile('./src/public/output/winTeamPerYear.json', winTeamPerYear, (error) => {
        if (error) {
            console.log(error);
        }
    });  

    return winTeamPerYear;
}

/**
 * This function accepts the data from matches.csv as array of object and returns the
 * match_Id of that match which was played in 2016.
 * 
 * @param {Array.<Object>} data : The data is from matches.csv as array of object
 * @return {Array} matchId : 2016's match Id
 */
let getMatchId_2016 = function(data){
    let matchId = [];
    if(data == undefined){
        return null;
    }

    data.forEach(row=>{
        if(row['season'] == 2016){
            matchId.push(row['id']);
        }
    });

    return matchId;
}

/**
 * This function accepts data from deliveries.csv as array of object and 
 * match Id as array. It generate json file of extra runs conceded 
 * per team in the year 2016 and also return this result as json string.
 * 
 * @param {Array.<Object>} data: data from deliveries.csv
 * @param {Array} matchId : match id of year 2016
 * @return {string}- result as json string
 */
let getExtraRuns = function(data, matchId){
    let teamsExtraRun = {};
    if(data == undefined || matchId == undefined){
        return null;
    }

    data.forEach(row => {  
        if(matchId.includes(row['match_id'])){
            if (row['bowling_team'] in teamsExtraRun){
                teamsExtraRun[row['bowling_team']] += parseInt(row['extra_runs']);
            }
            else{
                teamsExtraRun[row['bowling_team']] = parseInt(row['extra_runs']);
            }
        }
    });
    

    teamsExtraRun = JSON.stringify(teamsExtraRun);
    fs.writeFile('./src/public/output/teamsExtraRun.json', teamsExtraRun, (error) => {
        if (error) {
            console.log(error);
        }
    });

    return teamsExtraRun;
}

/**
 * This function accepts the data as array of object and returns the
 * match Id of that match which was played in 2015.
 * 
 * @param {Object.<string, number>} data : The data is from matches.csv as array of object
 * @return {Array} matchId : 2015's match Id 
 */
let getMatchId_2015 = function(data){
    let matchId = [];
    if(data == undefined){
        return null;
    }

    data.forEach(row=>{
        if(row['season'] == 2015){
            matchId.push(row['id']);
        }
    });

    return matchId;
}

/**
 * This function accepts data from deliveries.csv as array of object and 
 * match Id as array. It generate json file of top 10 economical 
 * bowlers in the year 2015 and also return this result as string.
 * 
 * @param {Array.<Object>} data: data from deliveries.csv
 * @param {Array} matchId: match id of year 2015
 * @return {string} bowler_obj- top 10 economical bowlers
 */
let getEconomicalBowlers = function(data,matchId){
    let bowlers = {};
    if(data == undefined || matchId == undefined){
        return null;
    }

    data.forEach(row => {   
        if(matchId.includes(row['match_id'])){
           if(row['bowler'] in bowlers){
                if(row['wide_runs'] != 0 || row['noball_runs'] != 0){
                    bowlers[row['bowler']]['runs'] += parseInt(row['total_runs']);
                }
                else{
                    bowlers[row['bowler']]['runs'] += parseInt(row['total_runs']);
                    bowlers[row['bowler']]['balls'] +=1 ;
                }
           }
           else{
               bowlers[row['bowler']] = {'runs':0,'balls':0};
           }
        }
    });

    let bowlerEconomy = [];
    Object.keys(bowlers).forEach(key => {
        let economy = (bowlers[key]['runs']/(bowlers[key]['balls']/6)).toFixed(2);
        let obj = {};
        obj[key] = economy;
        bowlerEconomy.push(obj);
    });

    bowlerEconomy.sort((a,b) => {
        return Object.values(a)[0]-Object.values(b)[0];
    })
    bowlerEconomy = bowlerEconomy.slice(0,10);

    let bowler_obj = {};
    bowlerEconomy.forEach(arg =>
        bowler_obj[(Object.keys(arg)[0])] = (Object.values(arg)[0])
    );

    bowler_obj = JSON.stringify(bowler_obj);
    fs.writeFile('./src/public/output/bowlerEconomy.json', bowler_obj, (error) => {
        if (error) {
            console.log(error);
        }
    });
    return bowler_obj;
}

module.exports={ 
    getMatchPerYear,
    getWinTeamPerYear,
    getExtraRuns,
    getMatchId_2016,
    getMatchId_2015,
    getEconomicalBowlers,
}