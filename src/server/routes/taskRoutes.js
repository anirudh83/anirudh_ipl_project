const express = require('express');
const { 
        getMatchPerYearData, 
        getWinTeamPerYear, 
        getTeamExtraRun,
        getBowlerEconomy 
       } = require('../getDataFromDB.js');


const route = express.Router();


route.get('/getMatchPerYear',(req,res)=>{
        getMatchPerYearData().then(data =>{
                res.status(200).send(data).end();
        })
        .catch(err =>{
                console.error(err);
        })
})

route.get('/getTeamExtraRuns',(req,res)=>{
        getTeamExtraRun().then(data =>{
                res.status(200).send(data).end();
        })
        .catch(err =>{
                console.error(err);
        })
})

route.get('/getWinTeamPerYear',(req,res)=>{
        getWinTeamPerYear().then(data =>{
                res.status(200).send(data).end();
        })
        .catch(err =>{
                console.error(err);
        })
})

route.get('/getBowlerEconomy',(req,res)=>{
        getBowlerEconomy().then(data =>{
                res.status(200).send(data).end();
        })
        .catch(err =>{
                console.error(err);
        })
})

module.exports = route;