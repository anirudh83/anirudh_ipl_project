const mysql = require('mysql');
const { con } = require('./db_conn.js');

function createTable(sqlQuery){
        return new Promise((resolve, reject) =>{
                con.query(sqlQuery, function (err, result) {
                        if (err){
                                reject(err);
                        }else{
                                resolve("table created");
                        }           
                });
        });
}

function insertDataIntoTable(data,tableName){
        let sql = `INSERT INTO ${tableName}  VALUES ?`;              
        con.query(sql, [data], function(err,result){
                if (err){
                        console.log(err);
                }else{
                        console.log("table created successfully");
                }          
        });
}

module.exports = { 
        createTable,
        insertDataIntoTable
}