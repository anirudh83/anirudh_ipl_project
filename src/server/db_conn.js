const mysql = require('mysql');
const dotenv = require('dotenv');

dotenv.config();
let con ;
if (process.env.CLEARDB_DATABASE_URL){
    con = mysql.createConnection(process.env.CLEARDB_DATABASE_URL)
}else{
    con = mysql.createConnection({
    host: process.env.DB_HOST,
    user: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_DATABASE
  });
}

module.exports ={ con }