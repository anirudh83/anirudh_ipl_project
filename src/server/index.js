const express = require('express');
const path = require('path');
const taskRoutes = require('./routes/taskRoutes')
require('dotenv').config();

const PORT = process.env.PORT || 3000;

const app = express();

app.use(express.static(path.join(__dirname, '../public')));

app.use('/task',taskRoutes);


app.listen(PORT, () => {
        console.log(`listening on port ${PORT}`);
})