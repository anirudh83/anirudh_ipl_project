const task1 = "https://ipl-analysis-anirudh.herokuapp.com/task/getMatchPerYear";
const task2 = "https://ipl-analysis-anirudh.herokuapp.com/task/getWinTeamPerYear";
const task3 = "https://ipl-analysis-anirudh.herokuapp.com/task/getTeamExtraRuns"
const task4 = "https://ipl-analysis-anirudh.herokuapp.com/task/getBowlerEconomy"

/**
 * getMatchPerYear fetch data from task1 url and 
 * show the statistics of number of matches played per year for all the years in IPL 
 * using Highcharts.
 * 
 */
function getMatchPerYear(){
  fetch(task1)
  .then(data=>{
    return data.json();
  })
  .then(data => {
      let total_match = [];
      let season = [];
      data.forEach(ob=>{
      season.push(ob['season']);
      total_match.push(ob['total_matches']);
      document.getElementsByClassName("center")[0].style.display = "none";

      Highcharts.chart('problem1-graph', {
        chart: {
          type: 'bar'
        },
        title: {
          text: 'Number of match per year in IPL'
        },
        subtitle: {
          text: 'Source: <a href="https://www.kaggle.com/manasgarg/ipl">Kaggle</a>'
        },
        xAxis: {
          categories: season,
          title: {
            text: 'Year'
          }
        },
        yAxis: {
          min: 0,
          title: {
            text: 'Number of matches',
            align: 'high'
          },
          labels: {
            overflow: 'justify'
          }
        },
        plotOptions: {
          bar: {
            dataLabels: {
              enabled: true
            }
          }
        },
        legend: {
          layout: 'vertical',
          align: 'right',
          verticalAlign: 'top',
          x: -40,
          y: 80,
          floating: true,
          borderWidth: 1,
          backgroundColor:
          Highcharts.defaultOptions.legend.backgroundColor || '#FFFFFF',
          shadow: true
        },
        credits: {
          enabled: false
        },
        series: [{
          name: 'No of matches',
          data: total_match
        }]
      }); 
    })
  })
  .catch(err=>{
    console.log(err);
  })
}

/**
 * getTeamData takes data form sql query (years wise) and change it into team wise  data.
 * and it convert full team name to short name. 
 * @param {Object.<number, {Object.<string,number>}>} data: data from winTeamPerYear.json
 * @return {Object.<string, Array<number>} dataTeamWise 
 */

function getTeamData(data){
    let teamsShortNames = {
      "Rising Pune Supergiants" : "RPS",
      "Mumbai Indians":"MI",
      "Kolkata Knight Riders":"KKR",
      "Delhi Daredevils":"DD",
      "Gujarat Lions":"GL",
      "Kings XI Punjab":"KXIP",
      "Sunrisers Hyderabad":"SH",
      "Royal Challengers Bangalore":"RCB",
      "Chennai Super Kings": "CSK",
      "Rajasthan Royals":"RR",
      "Deccan Chargers": "DD",
    }

    let dataTeamWise = {};
    let years = [2008,2009,2010,2011,2012,2013,2014,2015,2016];
    Object.keys(teamsShortNames).forEach(function(row){
      dataTeamWise[teamsShortNames[row]] = [];
      years.forEach(function(year){

        if(data[year][row] == undefined ){
          dataTeamWise[teamsShortNames[row]].push(0);
        }
        else{
          dataTeamWise[teamsShortNames[row]].push(data[year][row]);
        }
      })
    })
    return dataTeamWise;

}
/**
 * It fetch data from task2 url  and show the statistics of number 
 * of matches won per team per year in IPL using Highcharts.
 */

function getWinTeamPerYear(){
  fetch(task2).then(response => {
    return response.json();
  }).then(data => {
    document.getElementsByClassName("center")[1].style.display = "none";
    dataTeamWise = getTeamData(data);
    document.getElementById("problem2-graph").style.height = "600px";

    Highcharts.chart('problem2-graph', {
      chart: {
        type: 'bar'
      },
      title: {
        text: 'Number of matches won per team per year in IPL'
      },
      subtitle: {
        text: 'Source: <a href="https://www.kaggle.com/manasgarg/ipl">Kaggle</a>'
      },
      xAxis: {
        categories: [2008,2009,2010,2011,2012,2013,2014,2015,2016],
        title: {
          text: null
        }
      },
      yAxis: {
        min: 0,
        title: {
          text: 'No. of matches win',
          align: 'high'
        },
        labels: {
          overflow: 'justify'
        }
      },
      tooltip: {
        valueSuffix: ' millions'
      },
      plotOptions: {
        bar: {
          dataLabels: {
            enabled: true
          }
        }
      },
      legend: {
        layout: 'vertical',
        align: 'right',
        verticalAlign: 'top',
        x: -40,
        y: 80,
        floating: true,
        borderWidth: 1,
        backgroundColor:
        Highcharts.defaultOptions.legend.backgroundColor || '#FFFFFF',
        shadow: true
      },
      credits: {
        enabled: false
      },
      series: [{
        name: 'RPS',
        data: dataTeamWise['RPS']
      }, {
        name: 'MI',
        data: dataTeamWise['MI']
      }
      ,{
        name: 'KKR',
        data: dataTeamWise['KKR']
      }
      ,{
        name: 'DD',
        data: dataTeamWise['DD']
      },{
        name: 'MI',
        data: dataTeamWise['MI']
      },{
        name: 'GL',
        data: dataTeamWise['GL']
      },{
        name: 'KXIP',
        data: dataTeamWise['KXIP']
      },{
        name: 'SH',
        data: dataTeamWise['SH']
      },{
        name: 'RCB',
        data: dataTeamWise['RCB']
      },{
        name: 'CSK',
        data: dataTeamWise['CSK']
      },{
        name: 'RR',
        data: dataTeamWise['RR']
      },{
        name: 'DD',
        data: dataTeamWise['DD']
      }]
    });

  }).catch(err => {
    console.log(err);
  });
}

/**
 * getExtraRuns function fetch data from task3 url and show the graph 
 * of extra runs conceded per team in the year 2016 using Highcharts
 */
function getExtraRuns(){
  fetch(task3).then(response => {
    return response.json();
  }).then(data => {
    document.getElementsByClassName("center")[2].style.display = "none";
 
    Highcharts.chart('problem3-graph', {
      chart: {
        type: 'bar'
      },
      title: {
        text: 'Number of match per year in IPL'
      },
      subtitle: {
        text: 'Source: <a href="https://www.kaggle.com/manasgarg/ipl">Kaggle</a>'
      },
      xAxis: {
        categories: Object.keys(data),
        title: {
          text: 'Year'
        }
      },
      yAxis: {
        min: 0,
        title: {
          text: 'Number of matches',
          align: 'high'
        },
        labels: {
          overflow: 'justify'
        }
      },
    
      plotOptions: {
        bar: {
          dataLabels: {
            enabled: true
          }
        }
      },
      legend: {
        layout: 'vertical',
        align: 'right',
        verticalAlign: 'top',
        x: -40,
        y: 80,
        floating: true,
        borderWidth: 1,
        backgroundColor:
        Highcharts.defaultOptions.legend.backgroundColor || '#FFFFFF',
        shadow: true
      },
      credits: {
        enabled: false
      },
      series: [{
        name: 'Runs',
        data: Object.values(data)
      }]
    });
    
  }).catch(err => {
    console.log(err);
  });
}

/**
 * getEconomy function get data from task4 url and show the graph 
 * of Top 10 economical bowlers in the year 2015 using Highcharts.
 */
function getEconomy(){
  fetch(task4).then(response => {
    return response.json();
  }).then(data => {
    document.getElementsByClassName("center")[3].style.display = "none";
    let economy = [];
    Object.keys(data).forEach(row => {
      economy.push(parseFloat(data[row]));
    });

    Highcharts.chart('problem4-graph', {
      chart: {
        type: 'bar'
      },
      title: {
        text: 'Number of match per year in IPL'
      },
      subtitle: {
        text: 'Source: <a href="https://www.kaggle.com/manasgarg/ipl">Kaggle</a>'
      },
      xAxis: {
        categories: Object.keys(data),
        title: {
          text: 'Year'
        }
      },
      yAxis: {
        min: 0,
        title: {
          text: 'Number of matches',
          align: 'high'
        },
        labels: {
          overflow: 'justify'
        }
      },
    
      plotOptions: {
        bar: {
          dataLabels: {
            enabled: true
          }
        }
      },
      legend: {
        layout: 'vertical',
        align: 'right',
        verticalAlign: 'top',
        x: -40,
        y: 80,
        floating: true,
        borderWidth: 1,
        backgroundColor:
        Highcharts.defaultOptions.legend.backgroundColor || '#FFFFFF',
        shadow: true
      },
      credits: {
        enabled: false
      },
      series: [{
        name: 'bowler economy',
        data: economy
      }]
    });

  }).catch(err => {
    console.log(err);
  });
}
