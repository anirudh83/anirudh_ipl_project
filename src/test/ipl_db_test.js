const mysql = require('mysql');
const fs = require('fs'); 
const parse = require('csv-parser');

const { con } = require('../server/db_conn.js');

let { createTable, insertDataIntoTable } = require('../server/ipl_db.js');

let createMatchTableSql = `CREATE TABLE  IF NOT EXISTS matches (
        id int,
        season int,
        city varchar(255),
        date date,
        team1 varchar(255),
        team2 varchar(255) ,
        toss_winner varchar(255) ,
        toss_decision varchar(255),
        result varchar(255),
        dl_applied int,
        winner varchar(255),
        win_by_runs int,
        win_by_wickets int,
        player_of_match varchar(255),
        venue varchar(255),
        umpire1 varchar(255),
        umpire2 varchar(255),
        umpire3 varchar(255) 
)`;

let createDeliveriesTableSql = `CREATE TABLE IF NOT EXISTS deliveries (
        match_id int,
        inning int,
        batting_team varchar(100),
        bowling_team varchar(100),
        overs int,
        ball int,
        batsman varchar(100),
        non_striker varchar(100),
        bowler varchar(100),
        is_super_over int,
        wide_runs int,
        bye_runs int,
        legbye_runs int,
        noball_runs int,
        penalty_runs int,
        batsman_runs int,
        extra_runs int,
        total_runs int,
        player_dismissed varchar(100),
        dismissal_kind varchar(100),
        fielder varchar(100)
)`;

let getDeliveriesData = new Promise((resolve,reject) => {
        let data = [];
        fs.createReadStream('./src/data/deliveries.csv')
        .pipe(parse({ delimiter: ',' }))
        .on('data', (r) => {
                data.push(Object.values(r));        
        })
        .on('end', () => {
                if(data.length !== 0){
                        resolve(data); 
                }
                else{
                        reject(data);
                }
        });
});

let getMatchesData = new Promise((resolve,reject) => {
        let data = [];
        fs.createReadStream('./src/data/matches.csv')
        .pipe(parse({ delimiter: ',' }))
        .on('data', (r) => {
                data.push(Object.values(r));        
        })
        .on('end', () => {
                if(data.length !== 0){
                        resolve(data); 
                }
                else{
                        reject(data);
                }
        });
});

function createMatchesTable(){
        getMatchesData.then(data => {
                insertDataIntoTable(data,'matches'); 
        })
        .catch(err=>{
                console.log(err);
        })
}
function createDeliveriesTable(){
        getDeliveriesData.then(data=>{
                insertDataIntoTable(data,'deliveries');
        })
        .catch(err=>{
                console.log(err);
        })
}

createTable(createMatchTableSql).then(result=>{
        console.log(result);
        createMatchesTable();     
}).catch(err=>{
        console.log(err);
})

createTable(createDeliveriesTableSql).then(result=>{
        console.log(result);
        createDeliveriesTable();
}).catch(err=>{
        console.log(err);
})