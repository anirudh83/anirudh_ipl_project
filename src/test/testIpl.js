const parse = require('csv-parser')
const fs = require('fs'); 
const ipl =require('../server/ipl.js');

let getMatchesData = new Promise((resolve,reject) => {
        let data = [];
        fs.createReadStream('./src/data/matches.csv')
        .pipe(parse({ delimiter: ',' }))
        .on('data', (r) => {
                data.push(r);        
        })
        .on('end', () => {
                if(data.length !== 0){
                resolve(data); 
                }
                else{
                reject(data);
                }
        })

});
    
let getDeliveriesData = new Promise((resolve,reject) => {
        let data = [];
        fs.createReadStream('./src/data/deliveries.csv')
        .pipe(parse({ delimiter: ',' }))
        .on('data', (r) => {
                data.push(r);        
        })
        .on('end', () => {
                if(data.length !== 0){
                resolve(data); 
                }
                else{
                reject(data);
                }
        });
});

//problem 1
getMatchesData.then(data=>ipl.getMatchPerYear(data))
.catch(function(error){
    console.log(error);
})


//problem 2
getMatchesData.then(data=>ipl.getWinTeamPerYear(data))
.catch(function(error){
    console.log(error);
})

// problem 3
Promise.all([getMatchesData, getDeliveriesData]).then((values) => {
    let matchId=ipl.getMatchId_2016(values[0]);
    let extraRuns = ipl.getExtraRuns(values[1],matchId);
  });

//problem 4
Promise.all([getMatchesData, getDeliveriesData]).then((values) => {
    let matchId=ipl.getMatchId_2015(values[0]);
    let economicalBowlers = ipl.getEconomicalBowlers(values[1],matchId);
});